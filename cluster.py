
from pulumi import ResourceOptions
from pulumi_google_native.container.v1.cluster import Cluster, ClusterArgs, NodeConfigArgs
from pulumi_google_native.container.v1 import IPAllocationPolicyArgs, NodePoolArgs


my_cluster = Cluster(
    resource_name="bb-cluster1",
    args=ClusterArgs(
        name="bb-cluster1",
        location="us-west1-a",
        node_pools=[
            NodePoolArgs(
                name="bb-node-pool1",
                initial_node_count=3,
                config=NodeConfigArgs(
                    # by default these dont get populated, think we only need the devstorage one,
                    # but cant hurt to have the generally recommended stuff here -- esp for when
                    # we need to mount persistent volumes
                    oauth_scopes=[
                        "https://www.googleapis.com/auth/compute",
                        "https://www.googleapis.com/auth/devstorage.read_only",
                        "https://www.googleapis.com/auth/logging.write",
                        "https://www.googleapis.com/auth/monitoring",
                    ]
                ),
            )
        ],
        # use_ip_aliases sets the cluster to vpc-native, we statically assign the gke subnet too
        # because why not (also we need to use this subnet to update acl stuff)
        
        #ip_allocation_policy=IPAllocationPolicyArgs(
        #    services_ipv4_cidr_block=GKE_SERVICES_SUBNET,
        #    services_secondary_range_name="workshop-cluster-services",
        #    cluster_ipv4_cidr_block=GKE_PRIMARY_SUBNET,
        #    cluster_secondary_range_name="workshop-cluster-pods",
        #    create_subnetwork=False,
        #    use_ip_aliases=True,
        #),
        #network=workshop_vpc.id,
        #subnetwork="us-west1-primary",
    ),
    #opts=ResourceOptions(
    #    depends_on=[s for s in workshop_subnetworks.values()],  # noqa: pylint: disable=R1721
    #),
)