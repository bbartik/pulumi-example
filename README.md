## setup

NOTE: You must have already setup Pulumi config with a project.

```
$ PROJECT_NAME=<gcp-project-name>
$ git clone https://gitlab.com/bbartik/pulumi-example.git
$ cd pulumi-example
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements
$ pulumi stack init dev
$ pulumi config set gcp:project $PROJECT_NAME
$ pulumi config set google-native:project $PROJECT_NAME
$ pulumi config set google-native:zone us-west1-a
$ pulumi up
```

## after if you need kubectl

This populates the ~/.kube/config file

```
(venv) bbartik@bbartik-lenovo:/mnt/c/automation/pulumi$ gcloud container clusters get-credentials bb-cluster1
Fetching cluster endpoint and auth data.
kubeconfig entry generated for bb-cluster1.
```

