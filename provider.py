"""werkshop_deploy.gke.provider"""
from pulumi import Output, ResourceOptions
from pulumi_gcp.config import project, zone
from pulumi_kubernetes import Provider, ProviderArgs

from cluster import my_cluster

k8s_info = Output.all(
    my_cluster.name, my_cluster.endpoint, my_cluster.master_auth
)

k8s_config = k8s_info.apply(
    lambda info: """apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: {0}
    server: https://{1}
  name: {2}
contexts:
- context:
    cluster: {2}
    user: {2}
  name: {2}
current-context: {2}
kind: Config
preferences: {{}}
users:
- name: {2}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{{.credential.token_expiry}}'
        token-key: '{{.credential.access_token}}'
      name: gcp
""".format(
        info[2]["cluster_ca_certificate"], info[1], "{0}_{1}_{2}".format(project, zone, info[0])
    )
)

# Make a Kubernetes provider instance that uses our cluster from above.
k8s_provider = Provider(
    resource_name="gke_k8s",
    # TODO - play w/ the render to yaml cuz jfc waiitn for pulumi is the worst thing in the world
    #  https://www.pulumi.com/blog/kubernetes-yaml-generation/
    args=ProviderArgs(kubeconfig=k8s_config, suppress_helm_hook_warnings=True),
    # dont *think* this depends on *should* be necessary... but seems like it makes sense and
    # there were some issues w/ namespace deletion failing/being out of order...
    opts=ResourceOptions(depends_on=my_cluster),
)