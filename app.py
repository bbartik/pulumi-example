"""A Kubernetes Python Pulumi program"""
import pulumi

from pulumi import ResourceOptions

from pulumi_kubernetes.apps.v1 import Deployment, DeploymentSpecArgs
from pulumi_kubernetes.meta.v1 import LabelSelectorArgs, ObjectMetaArgs
from pulumi_kubernetes.core.v1 import ContainerArgs, PodSpecArgs, PodTemplateSpecArgs, \
    Service, ServiceSpecArgs, ServicePortArgs

from cluster import my_cluster
from provider import k8s_provider

clusterLocation = "us-west1-a"
app_labels = { "app": "bbapp1" }

deployment = Deployment(
    "bbdeploy",
    spec=DeploymentSpecArgs(
        selector=LabelSelectorArgs(match_labels=app_labels),
        replicas=2,
        template=PodTemplateSpecArgs(
            metadata=ObjectMetaArgs(labels=app_labels),
            spec=PodSpecArgs(containers=[ContainerArgs(name="bbtest", image="bbartik/bbtest-nginx",ports=[{"containerPort":80}])])
            ),
        ),
    metadata=ObjectMetaArgs(
        cluster_name=my_cluster.name,
        ),
    opts=ResourceOptions(provider=k8s_provider),
    )

service = Service(
    "bb-service",
    spec=ServiceSpecArgs(
        type="LoadBalancer",
        selector=app_labels,
        ports=[ServicePortArgs(name="nginx-port", port=8080, target_port=80)]
        ),
    metadata=ObjectMetaArgs(
        cluster_name=my_cluster.name,
        ),
    opts=ResourceOptions(provider=k8s_provider),
    )

pulumi.export("name", deployment.metadata["name"])
pulumi.export("loadBalancer_ip", service.status["load_balancer"]["ingress"][0]["ip"])
